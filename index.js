import express from "express";
import exphbs from 'express-handlebars';
import ProductRoutes from "./product/productRoutes.js"
import { getAllProducts } from "./product/productService.js"
const app = express();
const port = 8080;

const baseUrl = "/api";

import { Server } from "socket.io";
import http from "http";


const server = http.createServer(app);
const io = new Server(server);
app.locals.io = io;

io.on("connection", (socket) => {

  const products = getAllProducts();
  io.emit("initialization", { products });

  socket.on("new message", (msg) => {
    console.log("message: " + msg);
    io.emit("messages", msg);
  });

});

io.on('connection', (socket) => {

});

app.use(express.json())
app.use(express.static('public'))

app.use(baseUrl, ProductRoutes);

server.listen(port, () => {
  console.log('http listening on *:' + port);
});